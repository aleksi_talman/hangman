import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class wordlist {
    
    private List<String> wordlist = new ArrayList<>();

    public wordlist(String textfile) {
        try (Scanner syote = new Scanner(new File(textfile))) {
            while (syote.hasNextLine()) {
                wordlist.add(syote.nextLine());
            }
            syote.close();
        } catch (FileNotFoundException e) {
            System.out.println("Virhe tapahtui, väärä tiedostonimi");
            System.exit(0);
        }
    }

    public void setWordlist(List<String> wordlist) {
        this.wordlist = wordlist;
    }

    public List<String> annaSanat() {
        return this.wordlist;
    }

}
