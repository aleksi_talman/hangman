import java.util.Scanner;
import static java.lang.Character.toLowerCase;
import java.util.List;

public class Main {

    public static void main(String [] args) {
        Scanner syote = new Scanner(System.in);

        boolean tarkistus = false;

        do{
            try{
                System.out.println("Syötä sanalistan sistältävän tiedoston nimi: ");
                wordlist sanalista = new wordlist(syote.nextLine());
                List<String> sanat = sanalista.annaSanat();

                System.out.println("Arvauksien maksimi määrä on 6: ");
                System.out.println("Syötä arvauksien määrä: ");
                hangman hirsipuu = new hangman(sanat, syote.nextInt());
                syote.nextLine();

                System.out.println("Pelataanpas hieman hirsipuuta! Onnea matkaan...");
                System.out.println("Arvauksia on jaljella: " + hirsipuu.arvauksiaOnJaljella());

                while (hirsipuu.arvauksiaOnJaljella() > 0)  {
                    char arvaus = kysysyote();
                    boolean sisältyikö = hirsipuu.arvaa(arvaus);
                    if (sisältyikö) {
                        System.out.println("Kirjaimesi sisältyy löydettävään sanaan");

                        System.out.println("Kokeile arvata koko sana: ");
                        boolean sanan_arvaus = hirsipuu.onLoppu(syote.nextLine().strip().toLowerCase());
                        if (sanan_arvaus) {
                            System.out.println("Vastasit oikein! Voitit pelin!");
                            System.out.println("Etsimäsi sana oli:" + hirsipuu.sana());
                            break;
                        } else {
                            printHangman(hirsipuu.arvauksiaOnJaljella());
                            System.out.println("Vastasit väärin. Jatketaan...");
                        }
                    } else {
                        printHangman(hirsipuu.arvauksiaOnJaljella());
                        System.out.println("Kirjain ei sisälly etsimääsi sanaan. Jatketaan...");
                    }
                    System.out.println("Arvauksia on jäljellä: " + hirsipuu.arvauksiaOnJaljella());
                    System.out.println("Arvatut kirjaimet: " + hirsipuu.arvaukset());
                    System.out.println("Arvatut sanat: " + hirsipuu.getArvattusana());
                    if (hirsipuu.arvauksiaOnJaljella() == 0) {
                        System.out.println("Peli ohi. Arvaukset loppuivat.");
                        System.out.println("Etsimäsi sana oli:" + hirsipuu.sana());
                    }
                    tarkistus = true;
                }}catch(Exception e) {
                    System.out.println("Tapahtui virhe. Suljetaan peli.");
                    syote.close();
                    System.exit(0);
                    }}while (tarkistus != true);
    }

    public static Character kysysyote() {
        Scanner syote = new Scanner(System.in);
        boolean tarkistus = false;

        do{
            try{
                System.out.println("Arvaa kirjain: ");
                char letter = toLowerCase(syote.nextLine().charAt(0));
                tarkistus = true;
                return letter;
            } catch (Exception e) {
                System.out.println("Tapahtui virhe...");
                System.out.println("Suljetaan peli.");
                System.exit(0);
            }
        }while (tarkistus != true);
        syote.close();
            return null;
    }

    public static void printHangman(int arvauksia_jaljella) {
        if (arvauksia_jaljella == 6) {
            System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println("___|___");
			System.out.println();
        } else if (arvauksia_jaljella == 5) {
            System.out.println("   |");
			System.out.println("   |");
			System.out.println("   |");
			System.out.println("   |");
			System.out.println("   |");
			System.out.println("   |");
			System.out.println("   |");
			System.out.println("___|___");
        } else if (arvauksia_jaljella == 4) {
            System.out.println("   ____________");
			System.out.println("   |");
			System.out.println("   |");
			System.out.println("   |");
			System.out.println("   |");
			System.out.println("   |");
			System.out.println("   |");
			System.out.println("   | ");
			System.out.println("___|___");
        } else if (arvauksia_jaljella == 3) {
			System.out.println("   ____________");
			System.out.println("   |          _|_");
			System.out.println("   |         /   \\");
			System.out.println("   |        |     |");
			System.out.println("   |         \\_ _/");
			System.out.println("   |");
			System.out.println("   |");
			System.out.println("   |");
			System.out.println("___|___");
        } else if (arvauksia_jaljella == 2) {
            System.out.println("   ____________");
			System.out.println("   |          _|_");
			System.out.println("   |         /   \\");
			System.out.println("   |        |     |");
			System.out.println("   |         \\_ _/");
			System.out.println("   |           |");
			System.out.println("   |           |");
			System.out.println("   |");
			System.out.println("___|___");
        } else if (arvauksia_jaljella == 1) {
			System.out.println("   ____________");
			System.out.println("   |          _|_");
			System.out.println("   |         /   \\");
			System.out.println("   |        |     |");
			System.out.println("   |         \\_ _/");
			System.out.println("   |           |");
			System.out.println("   |           |");
			System.out.println("   |          / \\ ");
			System.out.println("___|___      /   \\");
        } else if (arvauksia_jaljella == 0) {
            System.out.println("   ____________");
			System.out.println("   |          _|_");
			System.out.println("   |         /   \\");
			System.out.println("   |        |     |");
			System.out.println("   |         \\_ _/");
			System.out.println("   |          _|_");
			System.out.println("   |         / | \\");
			System.out.println("   |          / \\ ");
			System.out.println("___|___      /   \\");
        }
    }
    
}