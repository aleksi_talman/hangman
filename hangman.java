import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class hangman {

    private List<Character> arvaukset = new ArrayList<>();
    private List<String> arvattusana = new ArrayList<>();
    private int arvausten_lkm;
    private String sana;
    private wordlist wordlist;

    public hangman(List<String> lista, int arvausten_lkm) {
        this.arvausten_lkm = arvausten_lkm;
        Random random = new Random();
        this.sana = lista.get(random.nextInt(lista.size())).strip().toLowerCase();
    }

    public List<String> getArvattusana() {
        return this.arvattusana;
    }

    public void setArvattusana(List<String> arvattusana) {
        this.arvattusana = arvattusana;
    }

    public List<Character> getArvaukset() {
        return this.arvaukset;
    }

    public void setArvaukset(List<Character> arvaukset) {
        this.arvaukset = arvaukset;
    }

    public void setArvausten_lkm(int arvausten_lkm) {
        this.arvausten_lkm = arvausten_lkm;
    }


    public void setSana(String sana) {
        this.sana = sana;
    }

    public wordlist getWordlist() {
        return this.wordlist;
    }

    public void setWordlist(wordlist wordlist) {
        this.wordlist = wordlist;
    }


    public boolean arvaa(Character merkki) {
        arvaukset.add(merkki);
        if (this.sana.contains(Character.toString(merkki))){
            return true;
        } else {
            this.arvausten_lkm--;
            return false;
        }
    }

    public List<Character> arvaukset() {
        return this.arvaukset;
    }

    public int arvauksiaOnJaljella() {
        return this.arvausten_lkm;
    }

    public String sana() {
        return this.sana;
    }

    public boolean onLoppu(String arvattu_sana) {
        arvattusana.add(arvattu_sana);
        if (arvattu_sana.matches(this.sana)) {
            return true;
        } else {
            return false;
        }
    }


    

}
